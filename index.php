<?php
/**
 * Plugin Name: Easy Visitor Counter
 * Plugin URI: http://mlk1.com/EVC
 * Version: 1.0
 * Author: Issam Abbas
 * Author URI: http://mlk1.com/
 * Description: A plugin that count the visitors in the site
 * License: GPL2
 * License URI :  https://www.gnu.org/licenses/gpl-2.0.html
 * 
 */


 /**
  * Adds EVC widget.
  */
 class EVC_Widget extends WP_Widget {

 	/**
 	 * Register widget with WordPress.
 	 */
 	function __construct() {
 		parent::__construct(
 			'EVC_widget', // Base ID
 			__( 'Easy Visitor Counter ', 'text_domain' ), // Name
 			array( 'description' => __( 'Display Easy Visitor Counter on the side bar or footer', 'text_domain' ), ) // Args
 		);
 	}

 	/**
 	 * Front-end display of widget.
 	 *
 	 * @see WP_Widget::widget()
 	 *
 	 * @param array $args     Widget arguments.
 	 * @param array $instance Saved values from database.
 	 */
 	public function widget( $args, $instance ) {
 		echo $args['before_widget'];
 		if ( ! empty( $instance['title'] ) ) {
 			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
 		}
?>
<img id='today' src="https://speedcounter.net/counter-v2/4012906-today-<?php echo $instance['color']  ?>.svg" alt="visitors today">
<img src="https://speedcounter.net/counter-v2/4012906-yesterday-<?php echo $instance['color']  ?>.svg" alt="visitors yesterday">
<img src="https://speedcounter.net/counter-v2/4012906-total-<?php echo $instance['color']  ?>.svg" alt="visitors total">
<img src="https://speedcounter.net/counter-v2/4012906-online-<?php echo $instance['color']  ?>.svg" alt="visitors online">

      <?php	echo $args['after_widget'];
      	}

 	/**
 	 * Back-end widget form.
 	 *
 	 * @see WP_Widget::form()
 	 *
 	 * @param array $instance Previously saved values from database.
 	 */
 	public function form( $instance ) {
 		$title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'Easy Visitor Counter', 'text_domain' );
    $min = ! empty( $instance['min'] ) ? $instance['min'] : __( '', 'text_domain' );
 		$color = ! empty( $instance['color'] ) ? $instance['color'] : __( '', 'text_domain' );
 		?>
 		<p>
 		<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( esc_attr( 'Title:' ) ); ?></label>
 		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
 		</p>
 		<p>
 	
 				<p>
 		<label for="<?php echo esc_attr( $this->get_field_id( 'color' ) ); ?>"><?php _e( esc_attr( 'color :' ) ); ?></label>
    <?php 'brightgreen' == $_GET['color'] ? ' selected ' : ''; ?>
 		<?php  $_GET['color'] == 'green' ? 'selected' : '' ;?>
<select label="Select color" id="<?php echo esc_attr( $this->get_field_id( 'color' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'color' ) ); ?>">
        <option value="brightgreen" <?php  $_GET['color'] == 'brightgreen' ? 'selected' : '' ;?>>brightgreen</option>
        <option value="green" <?php  $_GET['color'] == 'green' ? 'selected' : '' ;?>>green</option>
        <option value="yellow"  <?php  $_GET['color'] == 'yellow' ? 'selected' : '' ;?>>yellow</option>
        <option value="yellowgreen"  <?php  $_GET['color'] == 'yellowgreen' ? 'selected' : '' ;?>>yellowgreen</option>
        <option value="orange"  <?php  $_GET['color'] == 'orange' ? 'selected' : '' ;?>>orange</option>
        <option value="red"  <?php  $_GET['color'] == 'red' ? 'selected' : '' ;?>>red</option>
        <option value="blue"  <?php  $_GET['color'] == 'blue' ? 'selected' : '' ;?>>blue</option>
        <option value="gray"  <?php  $_GET['color'] == 'gray' ? 'selected' : '' ;?>>gray</option>
        <option value="lightgray"  <?php  $_GET['color'] == 'lightgray' ? 'selected' : '' ;?>>lightgray</option>
        <option value="pink"  <?php  $_GET['color'] == 'pink' ? 'selected' : '' ;?>>pink</option>
    </select>
 		</p>
 		<?php
 	}

 	/**
 	 * Sanitize widget form values as they are saved.
 	 *
 	 * @see WP_Widget::update()
 	 *
 	 * @param array $new_instance Values just sent to be saved.
 	 * @param array $old_instance Previously saved values from database.
 	 *
 	 * @return array Updated safe values to be saved.
 	 */
 	public function update( $new_instance, $old_instance ) {
 		$instance = array();
 		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    $instance['min'] = ( ! empty( $new_instance['min'] ) ) ? strip_tags( $new_instance['min'] ) : '';
 		$instance['color'] = ( ! empty( $new_instance['color'] ) ) ? strip_tags( $new_instance['color'] ) : '';

 		return $instance;
 	}

 } // class EVC_Widget




function EVC_setup_widget() {

  register_widget( 'evc_Widget' );
 
}
add_action( 'widgets_init', 'EVC_setup_widget' );
 
function pluginprefix_install() {
 
    // Trigger our function that registers the custom post type
    EVC_setup_widget();
 
}
register_activation_hook( __FILE__, 'EVC_install' );


function EVC_deactivation() {
 
 unregister_widget( 'evc_Widget'  );

}

register_deactivation_hook( __FILE__, 'EVC_deactivation' );

?>

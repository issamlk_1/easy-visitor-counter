=== Plugin Name ===
Contributors: issam abbas
plugin name: Easy Visitor Counter 
Donate link: http://www.mlk1.com
Tags: counter, visitor counter, counter visitors, count website visitors, count visitors, wordpress visitor counter, visitors counter, hit counter
Requires at least: 2.9.1
Tested up to: 4.5.3
Stable tag: EVC
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html



== Description ==

Visitor Counter Plugin to display Online, total, weekly and monthly visitor count. Count your traffic safely and show your visitors, with nice designe.




== Installation ==


1. Upload the plugin files to the `/wp-content/plugins/plugin-name` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Go to Appearance-> Widgets and drag the plugin to the desired widget area



== Frequently Asked Questions ==

= Is the Visitor Counter accurate? =

Yes 

= Does the plugin count unique visitors =

Yes



== Changelog ==

= 1.0 =



== Screenshots ==

1. The visitor counter widget in the sidebar on a website.



== Upgrade Notice ==

No upgrade needed



